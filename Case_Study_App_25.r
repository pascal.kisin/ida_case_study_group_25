if (!require(install.load)){
  install.packages("install.load")
}
library(install.load)
install_load("DT", "shiny", "tidyverse", "readr", "htmltools", "plotly", "shinyWidgets", "shinythemes", "leaflet", "leafpop", "ggplot2")

# Importing the dataset
filtered_data <- read.csv("Final_dataset_group_25.csv") %>%
  arrange(DistanceFromBerlin)  # Order rows by increasing distance



ui <- fluidPage(
  # Adding the light blue theme
  theme = shinytheme("cerulean"),
  # Adding the title panel with logo and title
  titlePanel(div("Case Study Group 25", tags$img(src = "logo.png", align = "right"))), ## Hier ist ein Fehler mit
  # Changing the font to Source Sans Pro
  tags$style(HTML("body{font-family: 'Source Sans Pro', sans-serif;}")),
  
  
  # Layout of the sidebar
  sidebarLayout(
    sidebarPanel(
      # Defining all user inputs
      numericInput("circle_count", "Number of Circles:", value = 1, min = 1, max = 10),
      uiOutput("sliders"),
      downloadButton("download_data", "Click the button to download the filtered data")
    ),
    
    # Layout of the main panel with different tabs
    mainPanel(
      tabsetPanel(
        type = "tabs",
        # First panel for the map 
        tabPanel("Map", leafletOutput("map", width="auto", height="1000px")),
        # Second panel for the interacitve bar chart
        tabPanel("Interactive Bar Chart", plotlyOutput("bar_chart")),
        # Third Panel for showing the dataset as Table
        tabPanel("Dataset", dataTableOutput("filtered_table"))
      ),
    )
  )
)




# Define server logic for the Shiny app
server <- function(input, output, session) {
  
  #render sliders with amount depending on user input
  output$sliders <- renderUI({
    lapply(1:input$circle_count,function(i){
      sliderInput(inputId = paste0("radius_scaling", i), label = paste("Radius Scaling", i),
                  min = 1, max = 80, value = 40)
    })
  })
  
  output$map <- renderLeaflet({
    # creates map
    map <- leaflet(data = filtered_data) %>%
      addTiles() %>%
      setView(lng = 13.4050, lat = 52.5200, zoom = 9) # Centered around Berlin
    
    # makes the amount of concentric circle the user chooses
    for (i in 1:input$circle_count) {
      map <- map %>% addCircles(lng = 13.4050, lat = 52.5200, radius = ~input[[paste0("radius_scaling", i)]]*1000, color = "purple", fillOpacity = 0.02) 
    }
    
    # Just add the markers, without popups.
    map <- map %>% addMarkers(lng = ~Laengengrad, lat= ~Breitengrad, group = "markerGroup")
    
    map
  })
  
  observeEvent(input$map_marker_click, {
    clicked_point <- input$map_marker_click
    
    if (!is.null(clicked_point)) {
      # Identify which data point was clicked based on lat-lon
      selected_row <- filtered_data[filtered_data$Laengengrad == clicked_point$lng & 
                                      filtered_data$Breitengrad == clicked_point$lat, ]
      
      # Create a popup plot for that data point
      df <- data.frame(group = c("affected", "not affected"), data = c(selected_row$affected, selected_row$total_diesel - selected_row$affected))
      
      p <- ggplot(df, aes(x = group, y = data, fill = group)) + 
        geom_bar(stat="identity", show.legend = FALSE) + 
        ylab("number of vehicles") + 
        xlab("")+ggtitle("Type11 vehicles \naffected by ban") + 
        theme(plot.title = element_text(hjust = 0.5, size = 11))
      
      popup_content <- paste("<strong>", "Total number of vehicles: ", selected_row$total, "</strong>", "<BR>", leafpop::popupGraph(p, type = "png", width = 175, height = 150))
      
      proxy <- leafletProxy("map")
      proxy %>% addPopups(lng=clicked_point$lng, lat=clicked_point$lat, popup = popup_content)
    }
  })
  
  
  
  
  # ... Task 4.b ...
  output$bar_chart <- renderPlotly({
    # Filtered data based on the largest selected radius
    largest_radius_data <- filtered_data %>%
      filter(DistanceFromBerlin <= input[[paste0("radius_scaling", input$circle_count)]])
    
    # Creating a summary table with the count of affected and unaffected vehicles for each radius
    summary_table <- lapply(1:input$circle_count, function(i) {
      radius_data <- filtered_data %>%
        filter(DistanceFromBerlin <= input[[paste0("radius_scaling", i)]])
      
      affected_count <- sum(radius_data$affected)
      unaffected_count <- sum(radius_data$total_diesel - radius_data$affected)
      
      data.frame(Radius = i, Affected = affected_count, Unaffected = unaffected_count)
    }) %>%
      bind_rows()
    
    # Creating the interactive bar chart
    plot_ly(summary_table, x = ~Radius, y = ~Affected, type = 'bar', name = 'Affected') %>%
      add_trace(y = ~Unaffected, name = 'Unaffected') %>%
      layout(
        title = "Number of Affected and Unaffected Diesel-Engined Vehicles",
        xaxis = list(title = "Radius"),
        yaxis = list(title = "Number of Vehicles")
      )
  })
  
  # ... Task 4.c and d ...
  
  # Render the filtered dataset as an interactive table
  output$filtered_table <- renderDataTable({
    filtered_data
  })
  
  # Download handler for the filtered data
  output$download_data <- downloadHandler(
    filename = function() {
      "filtered_data_80km.csv"
    },
    contentType = "text/csv",  # Specify content type as CSV
    content = function(file) {
      write.csv(filtered_data, file, row.names = FALSE)
    }
  )
}


# Run the Shiny app
shinyApp(ui = ui, server = server)


